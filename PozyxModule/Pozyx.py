#!/usr/bin/env python

import json
import time
import glob
import statistics
import math
from pypozyx import *

class Pozyx():
	def __init__(self,conf):

		self.conf = conf
		for port in get_serial_ports():
			try:
				if is_pozyx_port(port):
					self.pozyx = PozyxSerial(port.device)
			except:
				pass	
		# self.pozyx = PozyxSerial(self.pozyx_path)

		if (self.conf["pozyx_enable_self_reset"]):
			print("[INFO] Resetting Pozyx tag")
			self.pozyx.resetSystem()
			time.sleep(3)
			for port in get_serial_ports():
				try:
					if is_pozyx_port(port):
						self.pozyx = PozyxSerial(port.device)
				except:
					pass		
				
		self.remote_id = None
		self.num_of_anchors = self.conf["pozyx_num_of_anchors"]
		self.anchors_id = self.conf["pozyx_anchors_id"].split(',')
		self.anchors_id = [int(i,16) for i in self.anchors_id]


		# get Anchors position
		self.anchors_position_x = self.conf["pozyx_anchors_position_x"].split(',')
		self.anchors_position_x = [int(i) for i in self.anchors_position_x]
		self.anchors_position_y = self.conf["pozyx_anchors_position_y"].split(',')
		self.anchors_position_y = [int(i) for i in self.anchors_position_y]
		self.anchors_position_z = self.conf["pozyx_anchors_position_z"].split(',')
		self.anchors_position_z = [int(i) for i in self.anchors_position_z]
		
		# there is no self.pozyx.begin() as in Arduino library
		self.pozyx.clearDevices()
		self.setAnchorsManual()
		self.printCalibrationResult()
		self.groupSetTxPower()
		self.getTxInfo()

	def resetAll(self):
		for i in range(self.num_of_anchors):
			self.pozyx.resetSystem(remote_id=self.anchors_id[i])


	def groupSetTxPower(self):
		for i in range(self.num_of_anchors):
			self.pozyx.setTxPower(self.conf["pozyx_tx_gain"],remote_id = self.anchors_id[i])


	def getTxInfo(self):
		for i in range(self.num_of_anchors):
			tx = Data([0],"II")
			ret = self.pozyx.getTxPower(tx,remote_id = self.anchors_id[i])
			print("[INFO] Tx of 0x{} is : {}".format(format(self.anchors_id[i],'x'),tx))
		


	def setAnchorsManual(self):
		for i in range(self.num_of_anchors):
			anchor_position = Coordinates(self.anchors_position_x[i],
				self.anchors_position_y[i], self.anchors_position_z[i])
			anchor = DeviceCoordinates(self.anchors_id[i],0x1,anchor_position)

			if self.pozyx.addDevice(anchor,self.remote_id) is POZYX_SUCCESS:
				print("[SUCCESS] {} added.".format(anchor))


	def printCalibrationResult(self):

		list_size = Data([0],'B')
		status = self.pozyx.getDeviceListSize(list_size, remote_id = self.remote_id)
		print("")
		print("[INFO] Printing Calibration Result")
		print("[RESULT] List size: {0}".format(list_size[0]))

		if list_size[0] == 0:
			print("[RESULT] Calibration failed.\n%s" % self.pozyx.getSystemError())
			return

		device_list = DeviceList(list_size = list_size[0])
		status = self.pozyx.getDeviceIds(device_list, remote_id = self.remote_id)

		print("[RESULT] Anchors found: {0}".format(list_size[0]))
		print("[RESULT] Anchors' {}".format(device_list))

		for i in range(list_size[0]):
			anchor = Coordinates()
			status = self.pozyx.getDeviceCoordinates(device_list[i], anchor, self.remote_id)
			print("[RESULT] ANCHOR,0x%0.4x,%s" % (device_list[i], str(anchor)))


	def getRawPosition(self):
		position = Coordinates()
		status = self.pozyx.doPositioning(
			position, POZYX_3D, remote_id = self.remote_id)

		return position


	def getFilteredPosition(self):
		pos_x = []
		pos_y = []
		pos_z = []
		filtered_position = Coordinates()
		numberOfData = int(self.conf["pozyx_number_of_samples"])
		

		for i in range(numberOfData):
			position = self.getRawPosition()
			pos_x.append(position.x)
			pos_y.append(position.y)
			pos_z.append(position.z)
			time.sleep(self.conf["pozyx_refresh_lag"])

		filtered_position.x = self.removeOutlierAndGetAverage(pos_x)
		filtered_position.y = self.removeOutlierAndGetAverage(pos_y)
		filtered_position.z = self.removeOutlierAndGetAverage(pos_z)
		
		return filtered_position


	def removeOutlierAndGetAverage(self,data):
		threshold = self.conf["pozyx_data_threshold_level"]/100.0
		median = statistics.median(data)
		newData = []

		if (median >= 0):
			upperBound = median + (median * threshold)
			lowerBound = median - (median * threshold)

		else:
			upperBound = median - (median * threshold)
			lowerBound = median + (median * threshold)
			

		for num in data:
			if (num >= lowerBound) and (num <= upperBound):
				newData.append(num)

		if len(newData) == 0:
			return median

		return statistics.mean(newData)


	def printCoordinates(self, position):
		print("[INFO] x(mm): {}, y(mm): {}, z(mm): {}".format(position.x,position.y,position.z))

	def rotateCartesian(self,x,y):
		theta = math.radians(self.conf["pozyx_offset_cartesian_degree"])
		temp_x = x
		temp_y = y
		x = (temp_x * math.cos(theta)) + (temp_y * math.sin(theta))
		y = (-temp_x * math.sin(theta)) + (temp_y * math.cos(theta))

		return x,y
