#!/usr/bin/env python

class EMA:
	def __init__(self,alpha):
		self.alpha = alpha
		self.oldValue = None;

	def compute(self,value):
		if self.oldValue is None:
			self.oldValue = value
			return value

		newValue = self.oldValue + self.alpha * (value - self.oldValue)
		self.oldValue = newValue
		return newValue

