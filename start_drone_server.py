import os
import time
import json
import subprocess
from flask import Flask, render_template, request, jsonify
from random import randint
from threading import Thread
from collections import deque

os.chdir("/home/raynhl/dronescan_web_interface")

conf = json.load(open("main-config.json"))

while True:
	try:
		if conf["ssid_name"] in str(subprocess.check_output("iwgetid")):
			break
		else:
			pass
	except:
		print("[Sleeping] Not connected to {} network.".format(conf["ssid_name"]))
		time.sleep(2)

print("[SUCCESS] Wifi Connected: {}".format(conf["ssid_name"]))


app = Flask (conf["flask"]["app_name"], template_folder = "templates", static_folder = "static")

@app.route("/")
def main_view():
	return render_template("mainpage.html")

@app.route("/get_setup_options", methods=["POST"])
def get_setup_options():
	if request.method == "POST":
		data = request.get_json(silent=True)
		print(data["value"])

		anchors = []
		anchors_position_x = [int(i) for i in conf["pozyx"]["pozyx_anchors_position_x"].split(',')]
		anchors_position_y = [int(i) for i in conf["pozyx"]["pozyx_anchors_position_y"].split(',')]

		for i, x in enumerate(anchors_position_x):
			anchors.append(
				{
				"x": anchors_position_x[i],
				"y": anchors_position_y[i],
				"type": "Anchor"
				}
			)

		setup_data = {
			"chart_options": {
				"x_low" : min(anchors_position_x),
				"x_high": max(anchors_position_x),
				"y_low" : min(anchors_position_x),
				"y_high": max(anchors_position_y),
				"divisor": 20
			},
			"background_data": anchors + conf["features"]
		}

		return jsonify(setup_data)

@app.route("/get_current_data", methods=["POST"])
def get_current_data():
	if request.method == "POST":
		data = request.get_json(silent=True)
		print(data["value"])
		
		try:
			pos = data_queue.pop()

		except:
			position = {
				"valid": 0,
				"x": 0,
				"y": 0,
				"z": 0
			}

			return jsonify(position)

		position = {
			"valid": 1,
			"x": pos["x"],
			"y": pos["y"],
			"z": pos["z"]
		}

		if pos["x"] == 0 and pos["y"] == 0 and pos["z"] == 0:
			position["valid"] = 0

		return jsonify(position)



def get_drone_position(data_queue):
	global conf

	from PozyxModule.Pozyx import Pozyx
	from MovingAverageModule.MovingAverage import EMA
	
	pozyx = Pozyx(conf["pozyx"])
	ema_x = EMA(conf["pozyx"]["alpha_parameter"])
	ema_y = EMA(conf["pozyx"]["alpha_parameter"])
	ema_z = EMA(conf["pozyx"]["alpha_parameter"])

	while(1):
		pos = pozyx.getFilteredPosition()
		x = ema_x.compute(pos.x)
		y = ema_y.compute(pos.y)
		z = ema_z.compute(pos.z)

		position = {
		 	"x": x,
		 	"y": y,
		 	"z": z
		}

		# anchors_position_x = [int(i) for i in conf["pozyx"]["pozyx_anchors_position_x"].split(',')]
		# anchors_position_y = [int(i) for i in conf["pozyx"]["pozyx_anchors_position_y"].split(',')]

		# position = {
		#	"x": randint(0,max(anchors_position_x)),
		#	"y": randint(0,max(anchors_position_y)),
		#	"z": randint(0,400)
		#}

		data_queue.append(position)
		time.sleep(0.01)


data_queue = deque(maxlen = 1)

if conf["http_proxy"]:
	http_proxy = conf["http_proxy"] + ":911"
	https_proxy = conf["http_proxy"] + ":912"
	os.environ["http_proxy"] = http_proxy
	os.environ["HTTP_PROXY"] = http_proxy
	os.environ["https_proxy"] = https_proxy
	os.environ["HTTPS_PROXY"] = https_proxy

drone_position_thread = Thread(target = get_drone_position, args = (data_queue, ))
drone_position_thread.daemon = True
drone_position_thread.start()

app.run(
	host="0.0.0.0",
	port = conf["flask"]["port"],
	debug = True
	)
