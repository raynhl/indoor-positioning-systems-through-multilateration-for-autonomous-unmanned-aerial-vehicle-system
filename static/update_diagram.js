var GREEN = "#228B22"
var BLUE = "#3232ff"
var anchorSize = 100;
var droneSize = 400;

function getSetupOptions() {
	var xmlHttp = new XMLHttpRequest();   // new HttpRequest instance 
	xmlHttp.open("POST", "/get_setup_options", true);
	xmlHttp.setRequestHeader("Content-Type", "application/json");

	xmlHttp.onreadystatechange = function() {
		var obj = JSON.parse(this.response);
		var chartOptions = obj["chart_options"];
		backgroundData = obj["background_data"];

		var options = {
			showLine: false,
			width: "100%",
			height: "100%",
			axisX: {
				type: Chartist.FixedScaleAxis,
				low: Math.min(chartOptions["x_low"], chartOptions["y_low"]),
				high: Math.max(chartOptions["x_high"], chartOptions["y_high"]),
				divisor: chartOptions["divisor"],
			},
			axisY: {
				type: Chartist.FixedScaleAxis,
				low: Math.min(chartOptions["x_low"], chartOptions["y_low"]),
				high: Math.max(chartOptions["x_high"], chartOptions["y_high"]),
				divisor: chartOptions["divisor"],
			}
		};
		
		myChart.update(null, options);
		
	};

	xmlHttp.send(JSON.stringify({"value": "[Client] Requesting setup data"}));
}

function getCurrentData() {
	var xmlHttp = new XMLHttpRequest();   // new HttpRequest instance 
	xmlHttp.open("POST", "/get_current_data", true);
	xmlHttp.setRequestHeader("Content-Type", "application/json");

	xmlHttp.onreadystatechange = function() {
		var obj = JSON.parse(this.response);
		if (obj.valid == 1) {
			createGraph(obj);
		}
	};

	xmlHttp.send(JSON.stringify({"value": "[Client] Requesting current positional data"}));
}

function createGraph(currentData) {
	function getRandomInteger(min,max) {
		return Math.floor(Math.random() * (max - min + 1) ) + min;
	}

	var data = {
	series: [
		[
		{ x: currentData["x"], y: currentData["y"]}
		],
		backgroundData
	]
	};

	myChart.update(data);

	var dronePosStr = "X: " + currentData.x + " \tY: " + currentData.y;
	document.getElementById("drone_pos").innerHTML = dronePosStr; 
}

function createRectangle(posX, posY, width, height, fill, opacity, z, className, parent) {
	var attributes = {
		x: posX - (width / 2),
		y: posY - (height / 2),
		width: width,
		height: height,
		fill: fill,
		opacity: opacity,
		z: z
	};

	return Chartist.Svg('rect', attributes, className, parent, true);
}


var myChart = new Chartist.Line('.ct-chart', null, null);

var backgroundData;

myChart.on("draw", function(data) {
	if (data.type == "point" && data.seriesIndex == 1) {
		var shape, width, height;

		switch(data.series[data.index].type) {
			case "Anchor":
				var ratioX = (data.axisX.range.max - data.axisX.range.min) / data.axisX.axisLength;
				var ratioY = (data.axisY.range.max - data.axisY.range.min) / data.axisY.axisLength;	
				width = anchorSize / ratioX;
				height =anchorSize / ratioY;
				shape = createRectangle(data.x, data.y, width, height, GREEN, 1.0, -1, "anchor", data.element.parent());
				break;
			
			case "Rectangle":
				var ratioX = (data.axisX.range.max - data.axisX.range.min) / data.axisX.axisLength;
				var ratioY = (data.axisY.range.max - data.axisY.range.min) / data.axisY.axisLength;
				var width = data.series[data.index].width / ratioX;
				var height = data.series[data.index].height / ratioY;
				var shape = createRectangle(data.x, data.y, width, height, BLUE, 0.5, -1, "rectangle", data.element.parent());
				break;

			default:
				console.log("Not valid shape");

		}

		data.element.replace(shape);
	}

	if (data.type == "point" && data.seriesIndex == 0) {
		var ratioX = (data.axisX.range.max - data.axisX.range.min) / data.axisX.axisLength;
		var ratioY = (data.axisY.range.max - data.axisY.range.min) / data.axisY.axisLength;	
		var iconWidth = droneSize / ratioX;
		var iconHeight = droneSize / ratioY;

		var droneElem = document.createElement("img");
		droneElem.setAttribute("src", "static/drone_icon.png");
		droneElem.setAttribute("alt", "Drone Icon");
		droneElem.setAttribute("width", iconWidth);
		droneElem.setAttribute("height", iconHeight);

		var droneIcon = data.element.parent().foreignObject(droneElem, {
			width: iconWidth,
			height: iconHeight,
			x: data.x - iconWidth/2,
			y: data.y - iconHeight/2,
			z: 1,
			opacity: 1.0
		});

		data.element.replace(droneIcon);
	}
});

getSetupOptions();
setInterval(getCurrentData, 200);